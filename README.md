<!--- The following README.md sample file was adapted from https://gist.github.com/PurpleBooth/109311bb0361f32d87a2#file-readme-template-md by Gabriella Mosquera for academic use ---> 
<!--- You may delete any comments in this sample README.md file. If needing to use as a .txt file then simply delete all comments, edit as needed, and save as a README.txt file --->

# Lab 6 - 3172


* *Date Created*: 06 03 2023
* *Last Modification Date*: 06 03 2023
* *Lab URL*: <https://taupe-sable-a74db3.netlify.app/>
* *Git URL*: <https://gitlab.com/kennedylandry/3172-lab6/-/tree/main>

## Author

* [Kennedy Landry](kennedy.landry@dal.ca) - *Author*


## Acknowledgments

* This project uses data from the [OMDb API](http://www.omdbapi.com/), a free web service to obtain movie information.