import React, { useState } from 'react';
import axios from 'axios';
import './App.css';

function App() {
  const [searchTerm, setSearchTerm] = useState('');
  const [movies, setMovies] = useState([]);
  const [favorites, setFavorites] = useState([]);

  const handleSearch = () => {
    axios.get(`https://www.omdbapi.com/?s=${searchTerm}&apikey=85ca61e2`)
      .then((response) => {
        setMovies(response.data.Search);
      })
      .catch((error) => console.log(error));
  };

   const handleAddToFavorites = (movie) => {
    setFavorites([...favorites, movie]);
  };

  const handleRemoveFromFavorites = (movie) => {
    const updatedFavorites = favorites.filter((f) => f.imdbID !== movie.imdbID);
    setFavorites(updatedFavorites);
  };

  return (
    <div>
      <div className="main">
      <h1>Movie Search</h1>
      <div className="search-wrapper">
        <div className="search-container">
          <input
            type="text"
            placeholder="Search for a movie"
            value={searchTerm}
            onChange={(e) => setSearchTerm(e.target.value)}
          />
          <button onClick={handleSearch}>Search</button>
        </div>
      </div>
      <ul>
        {movies.map((movie) => (
          <li key={movie.imdbID}>
            <img src={movie.Poster} alt={movie.Title} />
            <h2>{movie.Title}</h2>
            <p>{movie.Year}</p>
            <button onClick={() => handleAddToFavorites(movie)}>Add to Favorites</button>
          </li>
        ))}
      </ul>

      <div className="favourite">
        <h2>Favorites</h2>
        <ul>
          {favorites.map((movie) => (
            <li key={movie.imdbID}>
              <img src={movie.Poster} alt={movie.Title} />
              <h3>{movie.Title}</h3>
              <button onClick={() => handleRemoveFromFavorites(movie)}>Remove</button>
            </li>
          ))}
        </ul>
      </div>
    </div>
    </div>
  );
}

export default App;